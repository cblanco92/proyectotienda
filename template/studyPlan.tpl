{include file="template/header.tpl" title="encabezado"}
<div id="content">
    <h2>{$language->translate('study_plans')}</h2>



    <div id="idTxt" style="background-color: lime; padding: 1em;">
        Nivel de estudios 
        <select id="levelSelect">
            <option value="0">-{$language->translate('select_one')}-</option>            
            {foreach $levels as $level}
                <option value="{$level.id}">{$level.nivel}</option>            
            {/foreach}
        </select>

        <br>    
        Estudios disponibles (v0:js + html)
        <select id="studySelect">
            <option>-{$language->translate('select_one')}-</option>            
        </select>

        <br>    
        Estudios disponibles (v1:jquery + html)
        <select id="studySelect2">
            <option>-{$language->translate('select_one')}-</option>            
        </select>

        <br>    
        Estudios disponibles (v2:jquery + json)
        <select id="studySelect3">
            <option>-{$language->translate('select_one')}-</option>            
        </select>
    </div>

</div>
{include file="template/footer.tpl" title="footer"}