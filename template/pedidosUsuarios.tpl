{include file="template/header.tpl" title="encabezado"}
<div id="content">
    <br>
    <h2>{$language->translate('pedidos')}</h2>

    <table>
        <tr>
            <th>{$language->translate('fechaEnviado')}</th>
            <th>{$language->translate('fechaServido')}</th>
            <th>{$language->translate('estado')}</th>
            <th>{$language->translate('usuario')}</th>
            <th>{$language->translate('producto')}</th>
        </tr>

        {foreach $pedidos as $pedido}
            <tr>
                <td>{$pedido.fechaPedido}</td>
                <td>{$pedido.fechaServido}</td>
                <td>{$pedido.estado}</td>
                <td>{$pedido.idUsuario}</td>
                <td>
                    <a href="#" class="myBtn" name="{$pedido.id}">Ver Detalle</a>                              
                </td>
            </tr>
        {/foreach}
    </table>

    <div id="myModal" class="modal">

        <!-- Modal content -->
        <div class="modal-content">
            <span class="close">×</span>
            <table >     
                <thead>
                    <tr>
                        <th>Nº</th>
                        <th>{$language->translate('name')}</th>
                        <th>{$language->translate('cantidad')}</th>
                        <th>{$language->translate('precio')}</th>
                        <th>Total</th>
                    </tr>
                </thead>

                <tbody id="detallePedido">

                </tbody>  
            </table></br>

            <div style="text-align:center">Total :<span id="total"></span> €</div>  
        </div>

    </div>
</div>
{include file="template/footer.tpl" title="footer"}