{include file="template/header.tpl" title="encabezado"}
<div id="content">
    <h2>{$language->translate('producto_list')}</h2>


    <div style="margin-bottom: 15px;">
        <label>{$language->translate('filtrarNombre')}: </label><br/><br/><input type="text" id="filtro" style="width: 100%">
        <br/><br/>
        <label>{$language->translate('filtrarPrecio')}: </label>
        <span id="valorMinimo" >0</span>
        <span>€ - </span>  
        <span id="valorMaximo" >300</span>
        <span>€</span>  
        <div id="slider" style="margin-top:15px;" ></div>

        <button style="margin-top:15px;text-align:center;width: 100%;" id="buscar" >{$language->translate('buscar')}</button>
    </div>
    {if $datosLogin.idRole ==3}
        <p><a href="{$url}{$lang}/producto/add">{$language->translate('new_producto')}</a></p>

    {/if}
    <table >     
        <thead>
            <tr>
                <th>{$language->translate('officialCode')}</th>
                <th>{$language->translate('name')}</th>
                <th>{$language->translate('precio')} <button id="menorMayor">{$language->translate('menor')}</button><button id="mayorMenor">{$language->translate('mayor')}</button></th>
                <th>{$language->translate('existencia')}</th>
                    {if $datosLogin.idRole !=1}
                    <th></th>

                {/if}
            </tr>
        </thead>

        <tbody id="prods">

        </tbody>  
    </table>
    <div id="paginacion" style="margin-top:15px;text-align:center;">



    </div>
    {if $datosLogin.idRole ==3}
        <p><a href="{$url}{$lang}/producto/add">{$language->translate('new_producto')}</a></p>

    {/if}

</div>
{include file="template/footer.tpl" title="footer"}