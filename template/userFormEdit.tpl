{include file="template/header.tpl" title="encabezado"}
<div id="content">
    <br>
    <h2></h2>

    <form action="{$url}{$lang}/user/update" method="post">
        <input type="hidden" name="id" value="{$row.id}">
        <label>{$language->translate('usuario')}</label><input type="text" name="name" value="{$row.nombre}"><br>
        <label>Role</label>

        {*        <input type="text" name="idRole" value="{$row.idRole}"> {$row.role}*}

        <select name="idRole" >
            {foreach $roles as $role}
                <option {($role.id==$row.idRole)? 'selected' : ''} 
                    value="{$role.id}">
                    {$role.role}
                </option>
            {/foreach}
        </select> 

        <br>



        <label>{$language->translate('contrasena')}</label><input type="password" name="password"> 
        <span class="error">{$language->translate($error.password)}</span> <br>
        <label></label><input type="submit" value="{$language->translate('submit')}">
    </form>

</div>
{include file="template/footer.tpl" title="footer"}