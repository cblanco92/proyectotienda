{include file="template/header.tpl" title="encabezado"}
<div id="content">
    <h2>{$language->translate('miPedido')}</h2>


    <table>
        <tr>
            <th hidden>Id</th>
            <th>{$language->translate('name')}</th>
            <th>{$language->translate('precio')}</th>
            <th>{$language->translate('cantidad')}</th>
            <th>Total</th>
            <th></th>
        </tr>
        <div hidden> {$contador=0}</div>
        {foreach $cesta as $art}
            <tr>
                <td hidden>{$art.idProducto}</td>
                <td>{$art.nombre}</td>
                <td>{$art.precio}</td>
                <td>{$art.cantidad}</td>
                <td>{$art.cantidad * $art.precio} €</td>
            <div hidden> {$total=$total+($art.cantidad * $art.precio)}</div>
            <td><a href="http://localhost/proyectoTienda/es/Pedido/borrar/{$contador}">Borrar</a></td>
            </tr>
            <div hidden> {$contador++}</div>      
        {/foreach}       

    </table></br>


    {if $total!=0}
        <div>Total :{$total} €</div>  
        <div style="text-align:center"> <a href="http://localhost/proyectoTienda/es/Pedido/guardar" onclick="alert('Compra realizada');">{$language->translate('enviar')}</a></div>   

    {/if}
</div>
{include file="template/footer.tpl" title="footer"}