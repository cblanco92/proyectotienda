{include file="template/header.tpl" title="encabezado"}
<div id="content">
    <br>
    <h2>{$language->translate('user_list')}</h2>

    <p><a href="{$url}{$lang}/user/add">{$language->translate('new_user')}</a></p>
    <table>
        <tr>
            <th>Id</th>
            <th>{$language->translate('name')}</th>
            <th>{$language->translate('contrasena')}</th>
            <th>{$language->translate('role')}</th>
            <th>{$language->translate('operations')}</th>
        </tr>

        {foreach $rows as $row}
            <tr>
                <td>{$row.id}</td>
                <td>{$row.nombre}</td>
                <td>{$row.password}</td>
                <td>{$row.role}</td>
                <td>
                    <a href="{$url}{$lang}/user/edit/{$row.id}" >editar</a>
                    <a href="{$url}{$lang}/user/delete/{$row.id}" >borrar</a>                    
                </td>
            </tr>
        {/foreach}
    </table>

    <p><a href="{$url}{$lang}/user/add">{$language->translate('new_user')}</a></p>

</div>
{include file="template/footer.tpl" title="footer"}