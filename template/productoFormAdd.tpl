{include file="template/header.tpl" title="encabezado"}
<div id="content">
    <br>
    <h2></h2>

    <form action="{$url}{$lang}/producto/insert" method="post">
        <label>{$language->translate('officialCode')} </label><input type="text" name="codigo"><br>
        <label>{$language->translate('name')}</label><input type="text" name="nombre"><br>
        <label>{$language->translate('price')}</label><input type="number" name="precio"><br>
        <label>{$language->translate('existencia')}</label><input type="number" name="existencia"><br>

        <label></label><input type="submit" value="{$language->translate('submit')}">
    </form>

</div>
{include file="template/footer.tpl" title="footer"}