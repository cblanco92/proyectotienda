<!DOCTYPE html>

<html>
    <head>
        <title>Cristian Blanco</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="{$url}public/css/default.css" />
        <script src="{$url}public/js/jquery.js" type="text/javascript"></script>   
        <script src="{$url}public/js/productos.js" type="text/javascript"></script> 
        <script src="{$url}public/js/pedidos.js" type="text/javascript"></script>        

        <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>


    </head>
    <body>
        <div id="header">
            <div id="title">
                Cristian Blanco DAW2
            </div>
            <div  style="float: left">
                <a href="{$url}{$lang}/index" >{$language->translate('index')}</a> 
                {if {$datosLogin.idRole}==3}
                    <a href="{$url}{$lang}/user" >{$language->translate('user')}</a>
                {/if}     
                <a href="{$url}{$lang}/producto" >{$language->translate('producto')}</a>

                {if {$datosLogin.idRole}==2}
                    <a href="{$url}{$lang}/pedido" >{$language->translate('miPedido')}</a> 
                    <a href="{$url}{$lang}/pedidosUsuarios" >{$language->translate('pedidos')}</a>                

                {/if}             

                {if {$datosLogin.idRole}==3}
                    <a href="{$url}{$lang}/pedidosUsuarios" >{$language->translate('pedidos')}</a>                
                {/if}

            </div>
            <div  style="float: left ; padding-left:2em">
                <a href="{$url}es/index" >ES</a> 
                <a href="{$url}en/index" >EN</a>
            </div>
            <div style="float:right;">
                <input type="hidden" id="idRole" value="{$datosLogin.idRole}"/>
                {if isset($datosLogin) && $datosLogin.nombre != "anonymous"}
                    {$datosLogin.nombre}
                    <a href="{$url}{$lang}/user/logout" >{$language->translate('logout')}</a>
                {else}

                    <a href="{$url}{$lang}/login" >{$language->translate('login')}</a>
                    <a href="{$url}{$lang}/login/registrar">{$language->translate('registrar')}</a>
                {/if}
            </div>
        </div>


