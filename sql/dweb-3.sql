-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 30-01-2016 a las 00:06:54
-- Versión del servidor: 5.6.17
-- Versión de PHP: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `dweb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno`
--

CREATE TABLE IF NOT EXISTS `alumno` (
  `numero` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apellidos` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `edad` int(2) DEFAULT NULL,
  `fechaMatricula` date NOT NULL,
  PRIMARY KEY (`numero`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=105 ;

--
-- Volcado de datos para la tabla `alumno`
--

INSERT INTO `alumno` (`numero`, `nombre`, `apellidos`, `edad`, `fechaMatricula`) VALUES
(2, 'MarÃ­a', 'SÃ¡nchez', 23, '2015-05-02'),
(10, 'Mariano', 'Cabeza', 33, '2010-11-30'),
(16, 'Pepe', 'afddafd', 23, '0000-00-00'),
(17, 'Mario', 'Bavarian', 13, '2015-09-01'),
(18, 'Pepe', 'Navarro Sola', 39, '0000-00-00'),
(19, 'Mario', 'Bavarian', 13, '2011-09-01'),
(102, 'Raquel', 'Roma GÃ³mez', 31, '2010-11-30'),
(104, 'Daniel', 'Villaverde PÃ©rez', 5, '2000-05-02');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estudio`
--

CREATE TABLE IF NOT EXISTS `estudio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codInterno` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `idNivel` int(11) DEFAULT NULL,
  `codOficial` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `codInterno` (`codInterno`),
  KEY `idNivel` (`idNivel`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `estudio`
--

INSERT INTO `estudio` (`id`, `codInterno`, `nombre`, `idNivel`, `codOficial`) VALUES
(3, 'DAM', 'Desarrollo de apllicaciones Multiplatafo', 5, 'IFC302'),
(4, 'DAW', 'Desarrollo de aplicaciones web', 5, 'IFC303'),
(6, 'GMIF', 'Sistemas MicroinformÃ¡ticos y redes', 4, 'IFC201'),
(7, 'GMBB', 'Grado medio basura cuadrado', 2, 'xxx000');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nivel`
--

CREATE TABLE IF NOT EXISTS `nivel` (
  `id` int(11) NOT NULL,
  `nivel` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `nivel`
--

INSERT INTO `nivel` (`id`, `nivel`) VALUES
(1, 'Secundaria Obligatoria'),
(2, 'FP Básica'),
(3, 'Bachillerato'),
(4, 'C.F. Grado Medio'),
(5, 'C.F. Grado Superior');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL,
  `role` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `role` (`role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `role`
--

INSERT INTO `role` (`id`, `role`) VALUES
(2, 'admin'),
(1, 'guest'),
(3, 'user');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idRole` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idRole` (`idRole`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `nombre`, `password`, `idRole`) VALUES
(3, 'Juan', 'd41d8cd98f00b204e9800998ecf8427e', 3),
(5, 'Rosa', 'e6ae4f9b46303106db282d1df173065f', 3),
(6, 'MarÃ­a', 'd41d8cd98f00b204e9800998ecf8427e', 2),
(7, 'pepe', '926e27eecdbc7a18858b3798ba99bddd', 2);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `estudio`
--
ALTER TABLE `estudio`
  ADD CONSTRAINT `estudio_ibfk_1` FOREIGN KEY (`idNivel`) REFERENCES `nivel` (`id`);

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `fk_user_role` FOREIGN KEY (`idRole`) REFERENCES `role` (`id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
