<?php

require_once 'lib/Controller.php';

class Index extends Controller {

    public function __construct() {
        parent::__construct('Index');
//        echo "Dentro de Index<br>";
    }

    public function __toString() {
        return 'INDEX';
    }

    public function method() {
        $this->view->setMethod("METHOD");
        $this->view->render();
    }

    public function hello($nombre = "", $apellido = "") {
        if (true) {
            echo "Hello $nombre $apellido <br>";
        } else
            throw new Exception;
    }

    public function index() {
        $this->view->render();
    }

}
