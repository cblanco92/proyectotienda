<?php

require_once 'lib/Controller.php';
require_once 'model/RoleModel.php';
require_once 'model/UserModel.php';

class Login extends Controller {

    public function __construct() {
        parent::__construct('Login');
//        echo "Dentro de Index<br>";
    }

    public function index() {

        $this->view->render();
    }

    public function error() {

        $this->view->error();
    }

    public function registrar() {
        $roleModel = new RoleModel();
        $roles = $roleModel->getRoleUsuario();
        $this->view->add($roles);
    }

    public function registro() {

        $row = $_POST;
        $userModel = new UserModel();
        $row['password'] = md5($row['password']);
        $userModel->insert($row);
        header('Location: ' . Config::URL . $_SESSION['lang'] . '/login');
    }

}
