<?php

require_once 'lib/Controller.php';
require_once 'model/LevelModel.php';

class Producto extends Controller {

    private $_levelModel = LevelModel;

    function __construct() {
        parent::__construct('Producto');
        $this->_levelModel = new LevelModel;
    }

    public function index() {
        //mostrar lista de todos los registros.
        //$rows = $this->model->getAll();
        $this->view->render($rows);
    }

    public function getAll($filtroNombre, $filtroMinimo, $filtroMaximo) {
        if ($filtroNombre != 'undefined' || $filtroNombre == '') {

            $productos = $this->model->getAllFiltro($filtroNombre, $filtroMinimo, $filtroMaximo);
        } else {
            $productos = $this->model->getAll();
        }
        echo json_encode($productos);
    }

    public function add($error = "") {
        $this->view->add($error);
    }

    public function insert() {
        $row = $_POST;
        $this->model->insert($row);
        header('Location: ' . Config::URL . $_SESSION['lang'] . '/producto');
    }

    public function delete($id) {
        $this->model->delete($id);
        header('Location: ' . Config::URL . $_SESSION['lang'] . '/producto');
    }

    public function edit($id, $error = "") {
        $row = $this->model->get($id);
        $this->view->edit($row, $error);
    }

    public function update() {
        $row = $_POST;

        if (count($error)) {
            $this->edit($row['id'], $error);
        } else {
            $this->model->update($row);
            header('Location: ' . Config::URL . $_SESSION['lang'] . '/producto');
        }
    }

    public function cesta() {
        $row = $_POST;
        $esta = false;
        $contador = 0;
        foreach ($_SESSION['cesta'] as $value) {

            if ($row['idProducto'] == $value['idProducto']) {
                $row['cantidad']+= $value['cantidad'];
                $esta = true;
                break;
            }
            $contador++;
        }
        if ($esta != false) {
            $_SESSION['cesta'][$contador] = $row;
        } else {
            $_SESSION['cesta'][] = $row;
        }
    }

}
