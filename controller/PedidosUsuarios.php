<?php

require_once 'lib/Controller.php';
require_once 'model/RoleModel.php';

class PedidosUsuarios extends Controller {

    public function __construct() {
        parent::__construct('PedidosUsuarios');
//        echo "Dentro de Index<br>";
    }

    public function index() {
        //mostrar lista de todos los registros.

        if ($_SESSION['datosLogin']['idRole'] == '3') {//admin
            $rows = $this->model->getAll();
        } else if ($_SESSION['datosLogin']['idRole'] == '2') {//usuario
            $rows = $this->model->getAllUsuario($_SESSION['datosLogin']['id']);
        }
        $this->view->render($rows);
    }

    public function getAllDetalle($idPedido) {
        $rows = $this->model->getAllDetallePedido($idPedido);

        echo json_encode($rows);
    }

    public function add() {
        
    }

    public function insert() {
        
    }

    public function delete($id) {
        
    }

    public function edit($id, $error = "") {
        
    }

    public function update() {
        
    }

}
