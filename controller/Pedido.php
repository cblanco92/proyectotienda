<?php

require_once 'lib/Controller.php';

class Pedido extends Controller {

    public function __construct() {
        parent::__construct('Pedido');
//        echo "Dentro de Help<br>";
    }

    public function index() {
//        echo "Metodo por defecto index";        
        $this->view->render($_SESSION['cesta']);
    }

    public function borrar($num) {

        unset($_SESSION['cesta'][$num]);
        header('Location: ' . Config::URL . $_SESSION['lang'] . '/Pedido');
    }

    public function guardar() {
        $this->model->guardarCesta($_SESSION['datosLogin'], $_SESSION['cesta']);
        unset($_SESSION['cesta']);
        header('Location: ' . Config::URL . $_SESSION['lang'] . '/Producto');
    }

}
