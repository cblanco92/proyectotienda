<?php

require_once 'lib/View.php';

class PedidoView extends View {

    function __construct() {
        parent::__construct();
//        echo 'En la vista Help<br>';
    }

    public function render($cesta, $plantilla = 'Pedido.tpl') {
        $this->smarty->assign('cesta', $cesta);
        $this->smarty->display($plantilla);
    }

}
