<?php

require_once 'lib/View.php';

class LoginView extends View {

    function __construct() {
        parent::__construct();
    }

    public function render($template = 'login.tpl') {
        $this->smarty->display($template);
    }

    public function error() {

        $this->smarty->assign('error', "Datos incorrectos.");
        $this->smarty->display('login.tpl');
    }

    public function add($roles) {
        $template = 'registro.tpl';
        $this->smarty->assign('roles', $roles);
        $this->smarty->display($template);
    }

}
