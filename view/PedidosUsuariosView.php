<?php

require_once 'lib/View.php';

class PedidosUsuariosView extends View {

    function __construct() {
        parent::__construct();
//        echo 'En la vista Index<br>';
    }

    public function render($pedidos, $template = 'pedidosUsuarios.tpl') {

        $this->smarty->assign('pedidos', $pedidos);
        $this->smarty->display($template);
    }

}
