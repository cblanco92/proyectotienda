<?php

require_once 'lib/View.php';

class ProductoView extends View {

    function __construct() {
        parent::__construct();
//        echo 'En la vista Index<br>';
    }

    public function render($rows, $template = 'producto.tpl') {
//        $this->smarty->assign('method', $this->getMethod());
        //$this->smarty->assign('rows', $rows);
        $this->smarty->display($template);
    }

    public function add($error = "") {
        $template = 'productoFormAdd.tpl';
        $this->smarty->display($template);
    }

    public function edit($row, $error = "") {
        $template = 'productoFormEdit.tpl';
        $this->smarty->assign('row', $row);
        $this->smarty->assign('error', $error);
        $this->smarty->display($template);
    }

}
