<?php
require_once 'lib/Model.php';

class PedidosUsuariosModel extends Model{

    function __construct()
    {
//        echo 'En el UserModel<br>'; 
        parent::__construct();
    }

    public function delete($id)
    {

    }

    public function get($id)
    {
       
    }

    public function getAll()
    {
        $this->_sql = "SELECT * from pedido  ";
        $this->executeSelect();
        return $this->_rows;
    }

    public function insert($fila)
    {
        
    }

    public function update($row)
    {
       
    }
    
    public function getAllUsuario($id){
        $this->_sql = "SELECT * from pedido where idUsuario=$id ";
        $this->executeSelect();
        return $this->_rows;        
    }
    public function getAllDetallePedido($idPedido){
        $this->_sql = "SELECT detallePedido.linea,producto.nombre,detallePedido.cantidad,detallePedido.precio"
                . " FROM detallePedido INNER JOIN producto "
                . " ON detallePedido.idProducto = producto.id   where idPedido=$idPedido";
              
        $this->executeSelect();
        return $this->_rows;        
    }

}