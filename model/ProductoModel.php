<?php
require_once 'lib/Model.php';

class ProductoModel extends Model{
    
    function __construct()
    {
//        echo 'En el UserModel<br>';
        parent::__construct();
    }

    public function delete($id)
    {
        $this->_sql = "DELETE FROM producto WHERE id=$id";
        $this->executeQuery();
    }

    public function get($id)
    {
        $this->_sql = "SELECT * FROM producto where id=$id ORDER BY id";
        $this->executeSelect();
        return $this->_rows[0];
    }


    public function getAll()
    {
        $this->_sql = "SELECT * from producto";
        $this->executeSelect();
        return $this->_rows;
    }
    public function getAllFiltro($filtroNombre,$filtroMinimo,$filtroMaximo)
    {
        if($filtroNombre=='null'){                 
              $this->_sql = "SELECT * from producto WHERE precio BETWEEN $filtroMinimo AND $filtroMaximo;";
        }else{
            $this->_sql = "SELECT * from producto WHERE nombre like '%$filtroNombre%' and  precio BETWEEN $filtroMinimo AND $filtroMaximo;";
        
        }
        $this->executeSelect();
        return $this->_rows;
    }

    public function insert($fila)
    {
        $this->_sql = "INSERT INTO producto(codigo, nombre, precio, existencia) "
                . "VALUES ('$fila[codigo]', '$fila[nombre]', '$fila[precio]', '$fila[existencia]')";

        $this->executeQuery();
    }

    public function update($row)
    {
        $this->_sql = "UPDATE producto SET "
                . " codigo='$row[codigo]', "
                . " nombre='$row[nombre]',"
                . " precio=$row[precio],"
                . " existencia='$row[existencia]'"
                . " WHERE id = $row[id]";
        $this->executeQuery();

    }

}