var ELEMENTOS_PAGINACION = 5;
var array_productos;
var paginas;
var idRole;
$(document).ready(function () {
    idRole = $("#idRole").val();
    loadDoc();
    $("#buscar").click(function () {
        loadDoc($("#filtro").val(), $("#valorMinimo").text(), $("#valorMaximo").text());
    });
    $("#menorMayor").click(function () {
        array_productos.sort(menorMayor);
        crearTabla();
    });
    $("#mayorMenor").click(function () {
        array_productos.sort(mayorMenor);
        crearTabla();
    });
    $("#slider").slider(
            {
                range: true,
                min: 0,
                max: 300,
                step: 1,
                values: [0, 300],
                change: showValue
            });

    function showValue(event, ui) {
        $("#valorMinimo").empty().append(ui.values[0] + " ");
        $("#valorMaximo").empty().append(ui.values[1] + " ");

    }
});

var loadDoc = function (filtroNombre, filtroMinimo, filtroMaximo) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            array_productos = JSON.parse(xhttp.responseText);
            //console.log(xhttp.responseText);
            crearTabla();

        }
    };
    if (filtroNombre == '')
    {
        xhttp.open("GET", "http://localhost/proyectoTienda/es/producto/getAll/" + null + "/" + filtroMinimo + "/" + filtroMaximo, true);
    } else {
        xhttp.open("GET", "http://localhost/proyectoTienda/es/producto/getAll/" + filtroNombre + "/" + filtroMinimo + "/" + filtroMaximo, true);

    }
    xhttp.send();
};
function crearTabla() {

    paginas = (array_productos.length / ELEMENTOS_PAGINACION);

    if ((array_productos.length % ELEMENTOS_PAGINACION) !== 0)
    {
        paginas++;
    }
    paginas = Math.floor(paginas);
    $("#paginacion").empty();
    for (var i = 1; i <= paginas; i++) {
        $("#paginacion").append("<button onclick=\"irPag('" + i + "');\" style='margin-right:15px;'>" + i + "</button>");
    }

    $("#prods").empty();
    for (var i = 0; i < ELEMENTOS_PAGINACION; i++) {
        var obj = array_productos[i];
        if (idRole == 1) {
            $("#prods").append("<tr><td>" + obj.codigo + "</td><td>" + obj.nombre + "</td>\n\
                        <td>" + obj.precio + "</td><td>" + obj.existencia + "</td></tr>");

        } else if (idRole == 2) {
            $("#prods").append("<tr><td>" + obj.codigo + "</td><td>" + obj.nombre + "</td>\n\
                        <td>" + obj.precio + "</td><td>" + obj.existencia + "</td><td><a href='' onclick=\"addProducto('" + obj.id + "','" + obj.nombre + "','" + obj.precio + "');\">Añadir a cesta</a></td></tr>");

        } else if (idRole == 3) {
            $("#prods").append("<tr><td>" + obj.codigo + "</td><td>" + obj.nombre + "</td>\n\
                        <td>" + obj.precio + "</td><td>" + obj.existencia + "</td><td><a style='margin-right:5px;' href='http://localhost/proyectoTienda/es/producto/edit/" + obj.id + " '>editar</a><a href='http://localhost/proyectoTienda/es/producto/delete/" + obj.id + " '>borrar</a></tr>");

        }
    }
}
function irPag(pag) {
    var ultimoElemento = pag * ELEMENTOS_PAGINACION;
    var primerElemento = ultimoElemento - ELEMENTOS_PAGINACION;
    $("#prods").empty();

    if (primerElemento < 0)
    {
        primerElemento = 0;
    }
    for (var i = primerElemento; i < ultimoElemento; i++) {
        var obj = array_productos[i];
        if (idRole == 1) {
            $("#prods").append("<tr><td>" + obj.codigo + "</td><td>" + obj.nombre + "</td>\n\
                        <td>" + obj.precio + "</td><td>" + obj.existencia + "</td></tr>");

        } else if (idRole == 2) {
            $("#prods").append("<tr><td>" + obj.codigo + "</td><td>" + obj.nombre + "</td>\n\
                        <td>" + obj.precio + "</td><td>" + obj.existencia + "</td><td><a href='' onclick=\"addProducto('" + obj.id + "','" + obj.nombre + "','" + obj.precio + "');\">Añadir a cesta</a></td></tr>");

        } else if (idRole == 3) {
            $("#prods").append("<tr><td>" + obj.codigo + "</td><td>" + obj.nombre + "</td>\n\
                        <td>" + obj.precio + "</td><td>" + obj.existencia + "</td><td><a style='margin-right:5px;' href='http://localhost/proyectoTienda/es/producto/edit/" + obj.id + " '>editar</a><a href='http://localhost/proyectoTienda/es/producto/delete/" + obj.id + " '>borrar</a></tr>");

        }
    }
}
function addProducto(idProducto, nombre, precio) {

    var cantidad = (function esNumero() {
        var n = prompt('Indica la cantidad que desea añadir a la cesta');
        return isNaN(n) || +n > 100000 || +n < 1 ? esNumero() : n;
    });


    $.ajax({
        type: 'POST',
        url: 'http://localhost/proyectoTienda/es/producto/cesta',
        data: {"idProducto": idProducto, "nombre": nombre, "precio": precio, "cantidad": cantidad},
        success: function (response) {
            alert("Producto añadido a la cesta");
        }
    }), 'json';


}

function menorMayor(a, b) {
    return a.precio - b.precio;
}
function mayorMenor(a, b) {
    return b.precio - a.precio;
}
