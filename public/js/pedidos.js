$(document).ready(function () {
    var modal = document.getElementById('myModal');
    var span = document.getElementsByClassName("close")[0];

    $(".myBtn").click(function () {
        var id = $(this).attr("name");
        modal.style.display = "block";
        loadDocPedido(id);
    });

    span.onclick = function () {
        modal.style.display = "none";
    }
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
});
var loadDocPedido = function (idPedido) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == 4 && xhttp.status == 200) {

            var array_detallePedido = JSON.parse(xhttp.responseText);

            $("#detallePedido").empty();
            var total = 0;
            for (var i = 0; i < array_detallePedido.length; i++) {
                var obj = array_detallePedido[i];
                $("#detallePedido").append("<tr><td>" + obj.linea + "</td><td>" + obj.nombre + "</td>\n\
                                <td>" + obj.cantidad + "</td><td>" + obj.precio + "</td><td>" + obj.cantidad * obj.precio + "</td></tr>");
                total += obj.cantidad * obj.precio;
            }
            $("#total").text(total);
        }
    };
    xhttp.open("GET", "http://localhost/proyectoTienda/es/pedidosUsuarios/getAllDetalle/" + idPedido, true);
    xhttp.send();
};
