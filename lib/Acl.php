<?php

/*
 * Clase con los permisos asiginados a un role:
 * Relación ROLE-RESOURCE
 */

//0, 1, 2

class Acl {

    private $_acl = array(
        'index' => array(
            'index' => 1,
            'hello' => 1
        ),
        'user' => array(
            'index' => 3,
            'add' => 3,
            'edit' => 3,
            'delete' => 3,
            'update' => 3,
            'insert' => 3
        ),
        'login' => array(
            'index' => 1,
            'registrar' => 1,
            'registro' => 1
        ),
        'pedido' => array(
            'index' => 2,
            'borrar' => 2,
            'guardar' => 2
        ),
        'pedidosUsuarios' => array(
            'index' => 2,
            'getAllDetalle' => 2
        ),
        'producto' => array(
            'index' => 1,
            'getAll' => 1,
            'add' => 3,
            'edit' => 3,
            'delete' => 3,
            'update' => 3,
            'insert' => 3,
            'cesta' => 2
        )
    );

    public function __construct($idUsuario) {
//        $this->_acl = cargarAclDelUsuario;
    }

    public function isAllowed($className, $method, $accessLevel) {
        $className = strtolower($className);
        if (isset($this->_acl[$className][$method])) {
            return $accessLevel >= $this->_acl[$className][$method];
        } else {
            return true;
        }
    }

}
