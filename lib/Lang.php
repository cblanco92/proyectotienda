<?php

class Lang {

    private $_langDefault = array();
    private $_langCurrent = array();

    function __construct() {
        //cargar arrays de traduccion
        $this->_langCurrent = $this->_loadLang($_SESSION['lang']);
        if ($_SESSION['lang'] != Config::DEFAULT_LANG) {
            $this->_langDefault = $this->_loadLang(Config::DEFAULT_LANG);
        }
    }

    private function _loadLang($lang) {
        require 'lang/' . $lang . '.php';
        return $$lang;
    }

    public function translate($key) {
        if (array_key_exists($key, $this->_langCurrent)) {
            $text = $this->_langCurrent[$key];
        } elseif (array_key_exists($key, $this->_langDefault)) {
            $text = $this->_langDefault[$key];
        } else
            $text = $key;
        return $text;
    }

}
